# Project1-CRUD Ops

This Java project dynamically handles DDL & DML operations. It allows you to dynamically create a table of arbitary size (variable no. of cols) and data types. Based on the datatype, an appropriate table is created which is manipulated via CRUD ops.